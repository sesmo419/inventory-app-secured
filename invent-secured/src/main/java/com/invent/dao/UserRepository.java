package com.invent.dao;


import org.springframework.data.jpa.repository.JpaRepository;

import com.invent.models.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
