package com.invent.dao;


import org.springframework.data.jpa.repository.JpaRepository;

import com.invent.models.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{
}
