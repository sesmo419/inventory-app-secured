<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>products</title>
    <link href="${contextPath}/resources/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/styles.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/custom-fonts.css" rel="stylesheet">
    <link href="${contextPath}/resources/fontawesome-5.8.1/css/all.css" rel="stylesheet"/>
    <script src="${contextPath}/resources/js/jquery-3.4.0.min.js"></script>  
  	<script src="${contextPath}/resources/bootstrap-3.3.7/js/bootstrap.min.js"></script>
  	<script src="${contextPath}/resources/js/axios.min.js"></script>
  	<script src="${contextPath}/resources/js/vue.js"></script>
  	<script src="${contextPath}/resources/js/invent-scripts.js"></script>
    <style>
    
	    .logoutbtn	{
			cursor:pointer;
		}
				
		#username:hover {
			color:white;
		}
		.wrapper {	
		  margin-top: 80px;
		  margin-bottom: 80px;
		}

		.form {
		  max-width: 380px;
		  padding: 15px 35px 45px;
		  margin: 0 auto;
		  background-color: #fff;
		  border: 1px solid rgba(0,0,0,0.1);  
		}
		.breadcrumb-wrapper{
			width:100%;
			height:60px;
			background: #6ca1f7;
		}
		.product-wrapper{
			width:100%;
			height:410px;
			margin-top:15px;
		}
		.add-product-section{
			background:#0f4668;
			height:410px;
			width:50%;
			float:left;
			border-left:10px solid #6ca1f7;
			text-align:center;
			border-radius: 0 20px 0 0;
		}
		#addProduct-form{
			margin-top:5px;
			width:80%;
			height:365px;;
			display:inline-block;
			background:#6893e8;
			border-bottom:2px solid white;
		
		}
		#updateProduct-form{
			margin-top:5px;
			width:80%;
			height:365px;;
			display:inline-block;
			background:#999cdb;
			border-bottom:2px solid white;
		
		}
		.addProductForm{
			padding-top:45px;
		}
		.updateProductForm{
			padding-top:45px;
		}
		.update-product-section{
			background:#1a577c;
			height:410px;
			width:50%;
			display:inline-block;
			border-left:5px solid #6ca1f7;
			border-right:10px solid #6ca1f7;	
			border-radius: 20px 0 0 0;	
			text-align:center;
		}
		#product-section{
			display:inline-block;
			background:white;
			border-radius:0 0 10px 10px;
			color:#02561e;
			font-size:16px;
			text-align:center;
			padding:5px;
			font-family:Times Roman;
			font-weight:bold;
		}
    </style>
</head>
<body>
  <div class="main-container">
    <c:if test="${pageContext.request.userPrincipal.name != null}">
        <form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>

    </c:if>
             <div id="navbar-wrapper">
			        <header>
			            <nav class="navbar navbar-default navbar-fixed-top " id="nav-bg" role="navigation">
			                <div class="container-fluid">
			                    <div class="navbar-header">
			                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
			                            <span class="sr-only">Toggle navigation</span>
			                            <span class="icon-bar"></span>
			                            <span class="icon-bar"></span>
			                            <span class="icon-bar"></span>
			                        </button>
			                        <a class="navbar-brand" id="logo" href="/home">Invent</a>
			                    </div>
			                    <div id="navbar-collapse" class="collapse navbar-collapse">
			                        <ul class="nav navbar-nav navbar-right">
			               
			                            <li class="dropdown">
			                                <a id="user-profile" href="#" class="dropdown-toggle" data-toggle="dropdown">
			                                	<img src="/resources/images/user.png" class="img-responsive img-thumbnail img-circle">
			                                		<span id="username">${pageContext.request.userPrincipal.name}</span></a>
			                                <ul class="dropdown-menu dropdown-block" role="menu">
			                                    <li><a href="#">Profile</a></li>
			                                    <li><a class="logoutbtn" onclick="document.forms['logoutForm'].submit()">Logout</a></li>
			                                </ul>
			                            </li>
			                        </ul>
			                    </div>
			                </div>
			            </nav>
			        </header>
			    </div>
			    <div id="wrapper">
			        <div id="sidebar-wrapper">
			            <aside id="sidebar">
			                <ul id="sidemenu" class="sidebar-nav">
				                <li id="sidebar-section-title">
				                        <a>
				                            <span class="sidebar-title">Main Navigation</span>
				                        </a>
				                </li>
			                    <li id="menu-item">
			                        <a href="/home">
			                            <span class="sidebar-icon"><i class="fas fa-tachometer-alt"></i></span>
			                            <span class="sidebar-title">DashBoard</span>
			                        </a>
			                    </li>
			                    <li id="menu-item">
			                        <a class="accordion-toggle collapsed toggle-switch" data-toggle="collapse" href="#submenu-1">
			                            <span class="sidebar-icon"><i class="fa fa-users"></i></span>
			                            <span class="sidebar-title">People</span>
			                            <b class="caret"></b>
			                        </a>
			                        <ul id="submenu-1" class="panel-collapse collapse panel-switch" role="menu">
			                            <li id="menu-sub-items"><a href="/manage-users"><i class="fa fa-caret-right"></i>Users</a></li>
			                            <li id="menu-sub-items"><a href="/manage-vendors"><i class="fa fa-caret-right"></i>Vendors</a></li>
			                        </ul>
			                    </li>
			                    <li id="menu-item">
			                        <a class="accordion-toggle collapsed toggle-switch" data-toggle="collapse" href="#submenu-2">
			                            <span class="sidebar-icon"><i class="fas fa-tools"></i></span>
			                            <span class="sidebar-title">General Settings</span>
			                            <b class="caret"></b>
			                        </a>
			                        <ul id="submenu-2" class="panel-collapse collapse panel-switch" role="menu">
			                            <li id="menu-sub-items"><a href="/company"><i class="fa fa-caret-right"></i>Company Info</a></li>
			                        </ul>
			                    </li>
			                    <li id="menu-item">
			                        <a class="accordion-toggle collapsed toggle-switch" data-toggle="collapse" href="#submenu-3">
			                            <span class="sidebar-icon"><i class="fab fa-product-hunt"></i></span>
			                            <span class="sidebar-title">Products</span>
			                            <b class="caret"></b>
			                        </a>
			                        <ul id="submenu-3" class="panel-collapse collapse panel-switch" role="menu">
			                            <li id="menu-sub-items"><a href="/add-product"><i class="fa fa-caret-right"></i>Add / Update Product</a></li>
			                            <li id="menu-sub-items"><a href="/manage-products"><i class="fa fa-caret-right"></i>Manage Products</a></li>
			                        </ul>
			                    </li>
			                     <li id="menu-item">
			                        <a class="accordion-toggle collapsed toggle-switch" data-toggle="collapse" href="#submenu-4">
			                            <span class="sidebar-icon"><i class="fas fa-coins"></i></span>
			                            <span class="sidebar-title">Sales</span>
			                            <b class="caret"></b>
			                        </a>
			                        <ul id="submenu-4" class="panel-collapse collapse panel-switch" role="menu">
			                            <li id="menu-sub-items"><a href="/sales"><i class="fa fa-caret-right"></i>New Sale</a></li>
			                            
			                        </ul>
			                    </li>
			                     <li id="menu-item">
			                        <a class="accordion-toggle collapsed toggle-switch" data-toggle="collapse" href="#submenu-5">
			                            <span class="sidebar-icon"><i class="fas fa-file-invoice-dollar"></i></span>
			                            <span class="sidebar-title">Invoices</span>
			                            <b class="caret"></b>
			                        </a>
			                        <ul id="submenu-5" class="panel-collapse collapse panel-switch" role="menu">
			                            <li id="menu-sub-items"><a href="/create-invoice"><i class="fa fa-caret-right"></i>Generate Invoice</a></li>
			                            <li id="menu-sub-items"><a href="/manage-invoices"><i class="fa fa-caret-right"></i>Invoice Management</a></li>
			                        </ul>
			                    </li>
			                    <li id="menu-item">
			                        <a class="accordion-toggle collapsed toggle-switch" data-toggle="collapse" href="#submenu-7">
			                            <span class="sidebar-icon"><i class="fas fa-cubes"></i></span>
			                            <span class="sidebar-title">Stock</span>
			                            <b class="caret"></b>
			                        </a>
			                        <ul id="submenu-7" class="panel-collapse collapse panel-switch" role="menu">
			                            <li id="menu-sub-items"><a href="/add-stock"><i class="fa fa-caret-right"></i>Add / Update Stock</a></li>
			                            <li id="menu-sub-items"><a href="/manage-stock"><i class="fa fa-caret-right"></i>Manage Stock</a></li>			                        </ul>
			                    </li>
			                    <li id="menu-item">
			                        <a href="/reports">
			                            <span class="sidebar-icon"><i class="far fa-chart-bar"></i></span>
			                            <span class="sidebar-title">Reports</span>
			                        </a>
			                    </li>
			                </ul>
			            </aside>            
			        </div>
			        <div id="content-wrapper">
				        	<div class="breadcrumb-section">
								<a href="/home" class="active" >Home</a>
							</div>
									            
			        </div>
							
			        <div id="footer">
			        <p>Copyright &copy;2019&nbsp;|&nbsp;All Rights Reserved</p>
			        </div>
			    </div> 
  
  </div>
  <script type="text/javascript">
 var app1 = new Vue({
	    el: '#addProduct-form',
	    data: function () {
	        return {
	        	products:Array,
                productName: '',
                productNo: '',
                quantity: '',
	            validationErrors: {
	                productName: null,
	                productNo: null,
	                quantity: null
	    	        
	            },
	            show:false
	        }

	    },
	    methods: {
	    	 
	        saveProduct:function() {
	            if (this.validateForm()) {
	                //submit form to backend
		   	           axios.post('/api/products/save',this.newProduct);
			   	      //show success Modal
			   	      this.show=true;
			   	      
	            }
	        },
	        validateForm:function() {
	            var formId = 'addProduct-form';
	            var nodes = document.querySelectorAll(`#${formId} :invalid`);
	            var errorObjectName = 'validationErrors';
	            var vm = this; //current vue instance;

	            Object.keys(this[errorObjectName]).forEach(key => {
	                this[errorObjectName][key] = null
	            });

	            if (nodes.length > 0) {
	                nodes.forEach(node => {
	                    this[errorObjectName][node.name] = node.validationMessage;
	                    node.addEventListener('change', function (e) {
	                        vm.validateForm();
	                    });
	                });

	                return false;
	            }
	            else {
	                return true;
	            }
	        },
	        closeModal:function(){
	  			this.show=false
	  		},
	  		resetForm:function(){
	    		this.productName='',
	    		this.productNo='',
	    		this.quantity='',
	    		this.show=false
	    	}
	    },
	    computed:{
	    	newProduct:function(){
	    		return {
	    			productName:this.productName,
	   	            productNo:this.productNo,
	   	            quantity:this.quantity
	    		}
	    	}
	    }
		 
	});
 var app2 = new Vue({
	    el: '#updateProduct-form',
	    data: function () {
	        return {
	         products:Array,
	         product:{},	
	         id:'',
             productName: '',
             productNo: '',
             processedQuantity:'',
	            validationErrors: {
	                productName: null,
	                productNo: null,
	                processedQuantity: null
	    	        
	            },
	            show:false
	        }

	    },
	    created: function () {
		      this.fetchProducts();
		    },
	    methods: {
	    	fetchProducts: function () {
	    	      axios.get('/api/products').then(function (response) {
	    	        this.products = response.data
	    	      }.bind(this)).catch(function (error) {
	    	        console.log('Error while fetching product data: ' + error)
	    	      })
	    	    },
	        updateProduct:function(product,processedQuantity,id){
	            if (this.validateForm()) {
		   	          axios.put(`/api/products/${product.id}/quantity?quantity=${processedQuantity}`);
			   	      //show success Modal
			   	      this.show=true
	            }
	        },
	        validateForm:function() {
	            var formId = 'updateProduct-form';
	            var nodes = document.querySelectorAll(`#${formId} :invalid`);
	            var errorObjectName = 'validationErrors';
	            var vm = this; //current vue instance;

	            Object.keys(this[errorObjectName]).forEach(key => {
	                this[errorObjectName][key] = null
	            });

	            if (nodes.length > 0) {
	                nodes.forEach(node => {
	                    this[errorObjectName][node.name] = node.validationMessage;
	                    node.addEventListener('change', function (e) {
	                        vm.validateForm();
	                    });
	                });

	                return false;
	            }
	            else {
	                return true;
	            }
	        },
	        closeModal:function(){
	  			this.show=false
	  		},

	    }
		 
	});


  
  </script>
  
</body>
</html>
